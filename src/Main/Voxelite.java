package Main;
import Entities.Player;
import Texture.Textures;
import World.Dimension;
import World.Dimensions.DimensionDefault;
import jdk.nashorn.internal.parser.Scanner;
import peasy.PeasyCam;
import processing.core.*;
import queasycam.QueasyCam;

import java.awt.*;


public class Voxelite extends PApplet {

    public static PApplet processing;
    public float eyeX, eyeY, eyeZ;
    private Player player;
    private Dimension dimension;
    public static Textures textures;

    public void draw() {
        //robot.mouseMove(width/2, height/2);
        //noCursor();
        background(176, 0, 0);
        pushMatrix();
        player.update();
        //camera(player.getX(), player.getY(), player.getZ(), player.getX()+eyeX, player.getY()-eyeY, player.getZ()+eyeZ, 0, 1, 0);
        rectMode(CENTER);
        fill(15, 15, 15);
        dimension.draw();
        popMatrix();
        pushMatrix();
        fill(255, 255, 255);
        textSize(16);
        text(Math.round(frameRate), 16, 16);
        popMatrix();
    }

    public void setup() {
        frameRate(120);
        processing = this;
        textures = new Textures();
        dimension = new DimensionDefault(3, 3, 3);
        dimension.generate();
        player = new Player(this);
        eyeY = player.getY() - 512;
        eyeX = player.getX() - 20;
        eyeZ = player.getZ() + 20;
    }

    public void keyPressed(){
//        player.getInput(key, keyCode, true);
    }
    public void keyReleased(){
//        player.getInput(key, keyCode, false);
    }

    public void mouseMoved(){
    }

    public void settings() {
        fullScreen();
        size(1920, 1080, P3D);
    }

    static public void main(String[] passedArgs) {
        String[] appletArgs = new String[]{"Main.Voxelite"};
        if (passedArgs != null) {
            PApplet.main(concat(appletArgs, passedArgs));
        } else {
            PApplet.main(appletArgs);
        }
    }

}