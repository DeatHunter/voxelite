package World.Dimensions;

import Main.Voxelite;
import World.Blocks.BlockAir;
import World.Blocks.BlockDirt;
import World.Blocks.BlockGrass;
import World.Blocks.BlockStone;
import World.Chunk;
import World.Dimension;

import java.util.ArrayList;
import java.util.Random;

public class DimensionDefault extends Dimension {

    public DimensionDefault(int sizeX, int sizeY, int sizeZ) {
        super(sizeX, sizeY, sizeZ);
    }

    private int maxSize = (Chunk.size * sizeY) * 3 / 8;
    private int minSize = (Chunk.size * sizeY) * 6 / 8;

    @Override
    public void generate() {
        Random rand = new Random();
        double nx = rand.nextInt(10000) * (rand.nextInt(1) >= 0.5 ? 1 : -1);
        this.lastY = new ArrayList<>();

        for(int x = 0; x < sizeX * Chunk.size; ++x) {
            nx += x > (sizeX * Chunk.size )/2 ? 0.025*20/sizeX : 0.075*20/sizeX;
            this.lastY.add(Math.round((this.maxSize - this.minSize) * Voxelite.processing.noise((float)nx) + this.minSize));
        }

        for (int i = 0; i < sizeY * Chunk.size; ++i) { //x
            for (int j = 0; j < sizeX * Chunk.size; ++j) { //y
                for (int k = 0; k < sizeZ * Chunk.size; ++k) { //z
                    int height = sizeY * Chunk.size - lastY.get(j);
                    if (i < height) {
                        this.setBlock(i, j, k, new BlockAir(j*32, i*32, k*32));
                    } else if (i == height) {
                        this.setBlock(i, j, k, new BlockGrass(j*32, i*32, k*32));
                    } else if (i < height + rand.nextInt(3) + 2) {
                        this.setBlock(i, j, k, new BlockDirt(j*32, i*32, k*32));
                    } else {
                        this.setBlock(i, j, k, new BlockStone(j*32, i*32, k*32));
//                    this.setBlock(j, i, new BlockStone(j, i));
                    }
                    if (i == sizeY * Chunk.size - 1) {
//                    this.setBlock(j, i, new BlockEnd(j, i));
                    }
                }
            }
        }

    }
}
