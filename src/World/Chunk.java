package World;

import World.Blocks.BlockAir;
import World.Blocks.BlockDirt;
import World.Blocks.BlockGrass;

import java.util.ArrayList;

public class Chunk {
    private float x;
    private float y;
    private float z;
    public static int size = 8;
    private ArrayList<ArrayList<ArrayList<Block>>> blocks;

    public Block getBlock(int x, int y, int z) {
        return this.blocks.get(z).get(y).get(x);
    }

    public void setBlock(int x, int y, int z, Block block) {
        Block newBlock = null;
        try {
            newBlock = (Block)block.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        this.blocks.get(z).get(y).set(x, newBlock);
    }

    public Chunk(float x, float y, float z){
        this.x = x;
        this.y = y;
        this.z = z;
        this.blocks = new ArrayList<>();
        for(int i = 0; i < size; ++i){
            ArrayList<ArrayList<Block>> face = new ArrayList<>();
            for(int j = 0; j < size; ++j){
                ArrayList<Block> line = new ArrayList<>();
                for(int k = 0; k < size; ++k){
                    line.add(new BlockAir(x+i*Block.size, y+j*Block.size, z+k*Block.size));
                }
                face.add(line);
            }
            this.blocks.add(face);
        }
    }

    public void draw(){
        for(ArrayList<ArrayList<Block>> face : this.blocks){
            for(ArrayList<Block> line : face){
                for(Block block : line){
                    block.draw();
                }
            }
        }
    }
}
