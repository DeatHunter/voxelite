package World;

import Main.Voxelite;
import Texture.Texture;

public class Block implements Cloneable {
    private float x;
    private float y;
    private float z;
    private String id = "none";
    private Texture topTex;
    private Texture botTex;
    private Texture leftTex;
    private Texture rightTex;
    private Texture frontTex;
    private Texture backTex;
    public static int size = 32;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Block(String id, float x, float y, float z){
        this.x = x;
        this.y = y;
        this.z = z;
        this.id = id;
    }

    public Block(String id, Texture topTex, Texture botTex, Texture leftText, Texture rightTex, Texture frontTex, Texture backTex, float x, float y, float z){
        this.x = x;
        this.y = y;
        this.z = z;
        this.id = id;
        this.topTex = topTex;
        this.botTex = botTex;
        this.leftTex = leftText;
        this.rightTex = rightTex;
        this.frontTex = frontTex;
        this.backTex = backTex;
    }

    public void draw(){

        if(id.equals("BlockAir"))return;
//        Voxelite.processing.fill(25, 75, 175);
        //BACK
        Voxelite.processing.beginShape();
            Voxelite.processing.texture(backTex.getImage());
            Voxelite.processing.vertex(this.x+size, this.y+size,this.z+0, 32, 32); //bot right
            Voxelite.processing.vertex(this.x+0, this.y+size,this.z+0, 0, 32); //bot left
            Voxelite.processing.vertex(this.x+0, this.y+0,this.z+0, 0, 0); //top left
            Voxelite.processing.vertex(this.x+size, this.y+0,this.z+0, 32, 0); //top right
        Voxelite.processing.endShape();

        //LEFT
        Voxelite.processing.beginShape();
            Voxelite.processing.texture(leftTex.getImage());
            Voxelite.processing.vertex(this.x+0, this.y+size,this.z+0, 0, 32); //bot left
            Voxelite.processing.vertex(this.x+0, this.y+size,this.z+size, 32, 32); //bot right
            Voxelite.processing.vertex(this.x+0, this.y+0,this.z+size, 32, 0); //top right
            Voxelite.processing.vertex(this.x+0, this.y+0,this.z+0, 0, 0); //top left
        Voxelite.processing.endShape();

        //BOTTOM
        Voxelite.processing.beginShape();
            Voxelite.processing.texture(botTex.getImage());
            Voxelite.processing.vertex(this.x+0, this.y+size,this.z+0, 0, 32); //bot left
            Voxelite.processing.vertex(this.x+0, this.y+size,this.z+size, 0, 0); //top left
            Voxelite.processing.vertex(this.x+size, this.y+size,this.z+size, 32, 0); //top right
            Voxelite.processing.vertex(this.x+size, this.y+size,this.z+0, 32, 32); //bot right
        Voxelite.processing.endShape();

        //RIGHT
        Voxelite.processing.beginShape();
            Voxelite.processing.texture(rightTex.getImage());
            Voxelite.processing.vertex(this.x+size, this.y+size,this.z+0, 0, 32); //bot left
            Voxelite.processing.vertex(this.x+size, this.y+size,this.z+size, 32, 32); //bot right
            Voxelite.processing.vertex(this.x+size, this.y+0,this.z+size, 32, 0); //top right
            Voxelite.processing.vertex(this.x+size, this.y+0,this.z+0, 0, 0); //top left
        Voxelite.processing.endShape();

        //TOP
        Voxelite.processing.beginShape();
            Voxelite.processing.texture(topTex.getImage());
            Voxelite.processing.vertex(this.x+size, this.y+0,this.z+size, 32, 32); //bot right
            Voxelite.processing.vertex(this.x+size, this.y+0,this.z+0, 32, 0); //top right
            Voxelite.processing.vertex(this.x+0, this.y+0,this.z+0, 0, 0); //top left
            Voxelite.processing.vertex(this.x+0, this.y+0,this.z+size, 0, 32); //bot left
        Voxelite.processing.endShape();

        //FRONT
        Voxelite.processing.beginShape();
            Voxelite.processing.texture(frontTex.getImage());
            Voxelite.processing.vertex(this.x+0, this.y+0,this.z+size, 0, 0); //top left
            Voxelite.processing.vertex(this.x+0, this.y+size,this.z+size, 0, 32); //bot left
            Voxelite.processing.vertex(this.x+size, this.y+size,this.z+size, 32, 32); //bot right
            Voxelite.processing.vertex(this.x+size, this.y+0,this.z+size, 32, 0); //top right
        Voxelite.processing.endShape();

        //0 0     32 0
        //0 32    32 32

    }
    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

}
