package World;

import World.Blocks.BlockAir;

import java.util.ArrayList;

public abstract class Dimension {
    protected int sizeX;
    protected int sizeY;
    protected int sizeZ;
    protected ArrayList<Integer> lastY;

    private ArrayList<ArrayList<ArrayList<Chunk>>> chunks;
    public abstract void generate();

    public Block getBlock(int x, int y, int z) {
        if(x >= 0 && x < sizeX*Chunk.size && y >= 0 && y < sizeY*Chunk.size)
            return this.chunks.get(z / Chunk.size).get(y / Chunk.size).get(x / Chunk.size).getBlock(x % Chunk.size, y % Chunk.size, z % Chunk.size);
        else
            return new BlockAir(0, 0, 0);
    }
    public void setBlock(int x, int y, int z, Block block){
        this.chunks.get(z / Chunk.size).get(y / Chunk.size).get(x / Chunk.size).setBlock(x % Chunk.size, y % Chunk.size, z % Chunk.size, block);
    }

    public Dimension(int sizeX, int sizeY, int sizeZ){
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        this.chunks = new ArrayList<>();
        for(int i = 0; i < sizeX; ++i){
            ArrayList<ArrayList<Chunk>> face = new ArrayList<>();
            for(int j = 0; j < sizeY; ++j){
                ArrayList<Chunk> line = new ArrayList<>();
                for(int k = 0; k < sizeZ; ++k){
                    line.add(new Chunk(i*Chunk.size*Block.size, j*Chunk.size*Block.size, k*Chunk.size*Block.size));
                }
                face.add(line);
            }
            this.chunks.add(face);
        }
    }

    public void draw(){
        for(ArrayList<ArrayList<Chunk>> face : this.chunks){
            for(ArrayList<Chunk> line : face){
                for(Chunk chunk : line){
                    chunk.draw();
                }
            }
        }
    }
}
