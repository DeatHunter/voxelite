package World.Blocks;

import Main.Voxelite;
import World.Block;

public class BlockDirt extends Block{
    public BlockDirt(float x, float y, float z) {
            super("BlockDirt", Voxelite.textures.getRandomTexture("blockDirt"), Voxelite.textures.getRandomTexture("blockDirt"), Voxelite.textures.getRandomTexture("blockDirt"), Voxelite.textures.getRandomTexture("blockDirt"), Voxelite.textures.getRandomTexture("blockDirt"), Voxelite.textures.getRandomTexture("blockDirt"), x, y, z);
    }
}
