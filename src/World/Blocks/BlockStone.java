package World.Blocks;

import Main.Voxelite;
import World.Block;

public class BlockStone extends Block {
    public BlockStone(float x, float y, float z){
        super("BlockStone", Voxelite.textures.getRandomTexture("blockStone"), Voxelite.textures.getRandomTexture("blockStone"), Voxelite.textures.getRandomTexture("blockStone"), Voxelite.textures.getRandomTexture("blockStone"), Voxelite.textures.getRandomTexture("blockStone"), Voxelite.textures.getRandomTexture("blockStone"), x, y, z);
    }
}
