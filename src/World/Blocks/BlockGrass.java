package World.Blocks;

import Main.Voxelite;
import World.Block;

public class BlockGrass extends Block{
    public BlockGrass(float x, float y, float z) {
            super("BlockGrass", Voxelite.textures.getRandomTexture("blockGrassTop"), Voxelite.textures.getRandomTexture("blockDirt"), Voxelite.textures.getRandomTexture("blockGrassSide"), Voxelite.textures.getRandomTexture("blockGrassSide"), Voxelite.textures.getRandomTexture("blockGrassSide"), Voxelite.textures.getRandomTexture("blockGrassSide"), x, y, z);
    }
}
