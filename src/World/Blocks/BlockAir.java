package World.Blocks;

import World.Block;

public class BlockAir extends Block {
    public BlockAir(float x, float y, float z) {
        super("BlockAir", x, y, z);
    }
}
