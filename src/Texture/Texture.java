package Texture;

import Main.Voxelite;
import processing.core.PImage;

public class Texture {
    PImage img;

    public Texture(String textureName) {
        this.img = Voxelite.processing.loadImage("../Resources/Textures/" + textureName);
    }
    public PImage getImage(){
        return this.img;
    }
}
