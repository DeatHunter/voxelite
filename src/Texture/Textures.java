package Texture;

import Main.Voxelite;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Textures {
    private HashMap<String, ArrayList<Texture>> textures;
    public Textures(){
        this.textures = new HashMap<>();
        java.io.File folder = new java.io.File(Voxelite.processing.dataPath("../Resources/Textures/"));

        String[] filenames = folder.list();

        assert filenames != null;
        for(String s : filenames) {
            String key = s.split("[_.]")[0];
            if (!this.textures.containsKey(key)) {
                this.textures.put(key, new ArrayList<Texture>());
            }
            this.textures.get(key).add(new Texture(s));
        }
    }

    public Texture getRandomTexture(String s){
        Random random = new Random();
        ArrayList<Texture> al = this.textures.get(s);
        return al.get(random.nextInt(al.size()));
    }
}
