package Entities;

import Main.Voxelite;
import processing.core.PApplet;
import processing.core.PVector;
import Libraries.QueasyCam;

public class Player extends QueasyCam implements Mob {
    private boolean forward, backward, left, right;
    private PVector pos;
    public Player(PApplet pp){
        super(pp);
        this.pos = new PVector(0, 512, 0);
    }

    public void update(){
        this.pos = position;
    }

    @Override
    public float getX() {
        return this.position.x;
    }
    @Override
    public float getY() {
        return this.position.y;
    }
    @Override
    public float getZ() {
        return this.position.z;
    }

    public void getInput(char key, int keyCode, boolean pressed){
    }

}
