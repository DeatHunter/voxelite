package Entities;

public interface Entity{
    float getX();
    float getY();
    float getZ();
}
